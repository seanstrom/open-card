# Open Card

Open Card is a powerful and agnostic credit card form / utility.
Open Card aims to build a complete and open experience for building a credit card form.

There are a lot of tools that provide amazing support for:
  * Credit validation
  * Payment provider integration
  * Overall good UI and UX

Open Card wants to bring together these ideas into a full product.  
Open Card will release with all the essential features, while the also providing break out of all the modules it will rely on.

Note: We will also support special integrations into JS frameworks by releasing a separate module.

## Roadmap
Open Card will start by using many different libraries together and overtime will replace components that need to be replaced for maintainability reasons or features.
